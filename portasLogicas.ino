//constantes
const byte LED_RED = 11;
const byte LED_BLUE = 12;
const byte LED_GREEN = 13;

/**
 * funções
 */
void Or(int);
void And(int);
void Nor(int);
void Nand(int);
void OrExclusivo(int);
void NorExclusivo(int);

void desligaLed()
{
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_BLUE, LOW);
  digitalWrite(LED_GREEN, LOW);
}

//setup
void setup()
{
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  Serial.begin(1500);
}

//executando em loop
void loop()
{
  int i, inputs[8][3] = {{0, 0, 0}, {0, 0, 1}, {0, 1, 0}, {0, 1, 1}, {1, 0, 0}, {1, 0, 1}, {1, 1, 0}, {1, 1, 1}};
  
  Serial.println();
  for (i = 0; i < 8; i++) {
    int vermelho = inputs[i][0];
    int azul = inputs[i][1];
    int verde = inputs[i][2];

    Serial.println(vermelho);
    Serial.println(azul);
    Serial.println(verde);

    Or(vermelho, azul, verde);
    delay(1000);
    desligaLed();

    And(vermelho, azul, verde);
    delay(1000);
    desligaLed();
    
    Nand(vermelho, azul, verde);
    delay(1000);
    desligaLed();
    
    Nor(vermelho, azul, verde);
    delay(1000);
    desligaLed();

    OrExclusivo(vermelho, azul, verde);
    delay(1000);
    desligaLed();

    NorExclusivo(vermelho, azul, verde);
    delay(1000);
    desligaLed();

    Serial.println();
  }

  delay(1000);
}

void Or(int vermelho, int azul, int verde)
{
    //basta que haja 1 entrada verdadeira para os posteriores serem verdadeiros
  Serial.println("Or. Nesta porta, basta que apenas 1 led esteja ligado para o verde ligar");
  if((vermelho + azul + verde) > 0) {
    digitalWrite(LED_RED, vermelho);
    digitalWrite(LED_BLUE, azul);
    digitalWrite(LED_GREEN, verde);
  }
}

void And(int vermelho, int azul, int verde)
{
    // entrada 1 e entrada 2 devem ser verdadeiras
  Serial.println("And. Nesta porta, para o led verde ligar, os led vermelho e azul devem estar ligados");
  if((vermelho * azul * verde) == 1) {
    digitalWrite(LED_RED, vermelho);
    digitalWrite(LED_BLUE, azul);
    digitalWrite(LED_GREEN, verde);
  }
}

void Nand(int vermelho, int azul, int verde)
{
    // entrada se todos forem 1 assume 0. Caso contrário, assume 1
  Serial.println("NAND...");
  if(!(vermelho * azul * verde)) {
    digitalWrite(LED_RED, HIGH);
    digitalWrite(LED_BLUE, HIGH);
    digitalWrite(LED_GREEN, HIGH);
  }
}

void Nor(int vermelho, int azul, int verde)
{
    // só vai assumir 1 quando as duas entradas forem 0
  Serial.println("NOR...");
  if(!((vermelho + azul + verde) > 0)) {
    digitalWrite(LED_RED, !vermelho);
    digitalWrite(LED_BLUE, !azul);
    digitalWrite(LED_GREEN, !verde);
  }
}

void OrExclusivo(int vermelho, int azul, int verde)
{
    //só vai assumir 1 quando a entrada 1 e a entrada 2 forem diferentes. Operadores de diferença
  Serial.println("OR EXCLUSIVE...");
  if((vermelho != azul) != verde) {
    digitalWrite(LED_RED, vermelho);
    digitalWrite(LED_BLUE, azul);
    digitalWrite(LED_GREEN, verde);
  }
}

void NorExclusivo(int vermelho, int azul, int verde)
{
    // quando a entrada 1 e entrada 2 forem iguais, assume-se 1. Operadores de igualdade
  Serial.println("NOR EXCLUSIVE...");
  if(!((vermelho != azul) != verde)) {
    digitalWrite(LED_RED, !vermelho);
    digitalWrite(LED_BLUE, !azul);
    digitalWrite(LED_GREEN, !verde);
  }
}
